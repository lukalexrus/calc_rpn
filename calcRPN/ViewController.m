//
//  ViewController.m
//  calcRPN
//
//  Created by Admin on 10/25/14.
//  Copyright (c) 2014 STC. All rights reserved.
//

#import "ViewController.h"
#import "math.h"


@interface ViewController (){
    double currentOperand;
    double operand1;
    BOOL dot;
    int power;
}

@property (weak, nonatomic) IBOutlet UILabel *tablo;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *digit;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *operations;
@property (weak, nonatomic) IBOutlet UIButton *clear;
@property (weak, nonatomic) IBOutlet UIButton *enter;
@property (weak, nonatomic) IBOutlet UIButton *dot;


@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    dot = NO;
    power = -1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)digit:(id)sender//обработка клавиш цифр
{
    //NSLog(@"digit");
    //UIButton *b=(UIButton*)sender;
    if(dot)
    {
        currentOperand=currentOperand+[sender tag]*pow(10,power);
        power--;
    }
    else{
        currentOperand=(currentOperand*10.0f)+[sender tag];
    }
    
    self.tablo.text=[NSString stringWithFormat:@"%f",currentOperand] ;
    
    
}

-(IBAction)dot:(id)sender
{
    dot=YES;
}

-(IBAction)clear:(id)sender//очитска поля
{
    self.tablo.text=@"0";
    currentOperand=0;
    dot=NO;
    power=-1;
}
-(IBAction)opertions:(id)sender//чтение операции
{
    UIButton *b=(UIButton*)sender;
    self.tablo.text=b.titleLabel.text;
    switch (b.tag) {//цифра с тега кнопки
        case 1://plus
            //NSLog(@"plus");
            currentOperand=operand1+currentOperand;
            break;
        case 2://minus
            currentOperand=operand1-currentOperand;
            break;
        case 3://multy
            currentOperand=operand1*currentOperand;
            break;
        case 4://div
            if(currentOperand!=0)
            {
            currentOperand=operand1/currentOperand;
            }
            break;
        case 5://-1
            currentOperand=1/currentOperand;
            break;
        case 6://pow
            currentOperand=pow(operand1,currentOperand);
            break;
        default:
            break;
    }
    self.tablo.text=[NSString stringWithFormat:@"%f",currentOperand] ;
}

-(IBAction)enter:(id)sender
{
    operand1=currentOperand;
    self.tablo.text=@"0";
    currentOperand=0;
    dot=NO;
    power=-1;
}

@end
