//
//  AppDelegate.h
//  calcRPN
//
//  Created by Admin on 10/25/14.
//  Copyright (c) 2014 STC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

